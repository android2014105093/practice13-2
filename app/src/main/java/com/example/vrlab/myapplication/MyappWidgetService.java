package com.example.vrlab.myapplication;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;

public class MyappWidgetService extends Service {

    public static final String UPDATE = "Update MyappWidget";

    @Override
    public void onStart(Intent intent, int startId) {
        String command = intent.getAction();
        int appWidgetId =
                intent.getExtras().getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);
        AppWidgetManager appWidgetManager =
                AppWidgetManager.getInstance(getApplicationContext());
        if (command.equals(UPDATE)) {
            MyappWidgetProvider.updateCount(
                    getApplicationContext().getPackageName(),
                    appWidgetManager, appWidgetId);
        }
        super.onStart(intent, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
