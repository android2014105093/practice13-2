package com.example.vrlab.myapplication;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.HashMap;

/**
 * Implementation of App Widget functionality.
 */
public class MyappWidgetProvider extends AppWidgetProvider {

    // 여러 앱위젯들의 카운트를 보관하기 위한 해쉬맵
    private static HashMap<Integer, Integer> mCountMap =
            new HashMap<Integer, Integer>();
    // 여러 앱위젯들의 업데이트 인텐트를 보관하기 위한 해쉬맵
    private static HashMap<Integer, PendingIntent> mActionMap;
    // 갱신 주기
    public static int mUpdateInterval;

    // 특정 앱위젯(ID 번호로 구분)의 카운트 내용을 갱신
    public static void updateCount(String packageName,
                                   AppWidgetManager appWidgetManager, int appWidgetId) {
        // 카운트 얻기
        Integer count = mCountMap.get(appWidgetId);
        if (count == null)
            count = 0;
        // 출력 내용 작성
        String output;
        output = "Count: " + count;
        int resid;
        if ((count % 2) == 0)
            resid = R.drawable.heart2;
        else
            resid = R.drawable.heart1;

        // 카운트 증가 후 보관
        count++;
        mCountMap.put(appWidgetId, count);
        // 앱위젯의 화면 구성 설정
        RemoteViews views = new RemoteViews(packageName,
                R.layout.myapp_widget_provider);
        views.setImageViewResource(R.id.imageView1, resid);
        views.setTextViewText(R.id.textview1, output);
        // 관리자에게 업데이트를 지시
        appWidgetManager.updateAppWidget( appWidgetId, views );
    }

    public static PendingIntent makeUpdateAction(Context context, String command, int appWidgetId) {
        // 앱위젯ID를 가진 인텐트 생성
        Intent active = new Intent(context, MyappWidgetService.class);
        active.setAction(command);
        active.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        // 생성된 인텐트를 Unique하게 만들어 주기 위해서 Uri를 추가
        // 실제로는 사용되지 않는 부가 정보이지만 Uri를 추가함으로 해서
        // 해당 앱위젯ID의 유일한 인텐트로 만들어줄 수 있다.
        // 이 코드가 주석처리된다면 복수개의 앱위젯 중
        // 마지막에 추가된 위젯만 갱신되는 결과를 보인다.
        Uri data = Uri.withAppendedPath(
                Uri.parse("myappwidgetservice://widget/id/#" + command + appWidgetId),
                String.valueOf(appWidgetId));
        active.setData(data);
        // 업데이트 서비스를 구동시키기 위한 펜딩인텐트 생성
        PendingIntent action = PendingIntent.getService(
                context, 0, active, PendingIntent.FLAG_UPDATE_CURRENT);
        return action;
    }

    public static void setAlarm(Context context,
                                int updateInterval, PendingIntent action) {
        AlarmManager alarmManager = (AlarmManager)
                context.getSystemService(Context.ALARM_SERVICE);
        if (updateInterval > 0) {
        // 주어진 갱신 주기마다 반복적인 알람 추가
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(), updateInterval, action);
        } else {
        // 알람 제거
            alarmManager.cancel(action);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("AndroidRuntime", "MyAppWidgetProvider.onReceive");
        super.onReceive(context, intent);
    }

    @Override
    public void onEnabled(Context context) {
        Log.i("AndroidRuntime", "MyAppWidgetProvider.onEnabled");
        // 앱위젯이 최초 추가될 때 인텐트용 해쉬맵 생성
        mActionMap = new HashMap<Integer, PendingIntent>();
        // 기본 갱신 주기는 1초
        mUpdateInterval = 1000;
        super.onEnabled(context);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.i("AndroidRuntime", "MyAppWidgetProvider.onUpdate");
        // 업데이트가 발생한 모든 앱위젯
        for (int appWidgetId : appWidgetIds) {
        // 카운트 갱신
            updateCount(context.getPackageName(), appWidgetManager, appWidgetId);

            // 갱신 타이머 등록
            PendingIntent action;
            action = mActionMap.get(appWidgetId);
            if (action == null) {
                action = makeUpdateAction(context, MyappWidgetService.UPDATE, appWidgetId);
                mActionMap.put(appWidgetId, action);
                setAlarm(context, mUpdateInterval, action);
            }
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        Log.i("AndroidRuntime", "MyAppWidgetProvider.onDeleted");
        // 제거가 진행되는 모든 앱위젯
        for (int appWidgetId : appWidgetIds) {
        // 갱신 타이머 해제
            PendingIntent action = mActionMap.get(appWidgetId);
            if (action != null) {
                setAlarm(context, 0, action);
                mActionMap.remove(appWidgetId);
            }
        }
        super.onDeleted(context, appWidgetIds);
    }
    @Override
    public void onDisabled(Context context) {
        Log.i("AndroidRuntime", "MyAppWidgetProvider.onDisabled");
        // 모든 앱위젯이 제거되면 실행중인 서비스 중지
        context.stopService(new Intent(context,MyappWidgetService.class));
        // 인텐트용 해쉬맵 제거
        mActionMap.clear();
        mActionMap = null;
        super.onDisabled(context);
    }
}

